<?php

namespace App\Controller\Admin;

use App\Entity\Cartoon;
use App\Form\CartoonType;
use App\Repository\CartoonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/cartoon")
 */
class CartoonController extends AbstractController
{
    /**
     * @Route("/", name="cartoon_index", methods={"GET"})
     */
    public function index(CartoonRepository $cartoonRepository): Response
    {
        return $this->render('admin/cartoon/index.html.twig', [
            'cartoons' => $cartoonRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="cartoon_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $cartoon = new Cartoon();
        $form = $this->createForm(CartoonType::class, $cartoon);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($cartoon);
            $entityManager->flush();

            return $this->redirectToRoute('cartoon_index');
        }

        return $this->render('admin/cartoon/new.html.twig', [
            'cartoon' => $cartoon,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="cartoon_show", methods={"GET"})
     */
    public function show(Cartoon $cartoon): Response
    {
        return $this->render('admin/cartoon/show.html.twig', [
            'cartoon' => $cartoon,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="cartoon_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Cartoon $cartoon): Response
    {
        $form = $this->createForm(CartoonType::class, $cartoon);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cartoon_index');
        }

        return $this->render('admin/cartoon/edit.html.twig', [
            'cartoon' => $cartoon,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="cartoon_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Cartoon $cartoon): Response
    {
        if ($this->isCsrfTokenValid('delete' . $cartoon->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($cartoon);
            $entityManager->flush();
        }

        return $this->redirectToRoute('cartoon_index');
    }
}
