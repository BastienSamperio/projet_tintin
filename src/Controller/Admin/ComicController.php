<?php

namespace App\Controller\Admin;

use App\Entity\Comic;
use App\Form\ComicType;
use App\Repository\ComicRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/comic")
 */
class ComicController extends AbstractController
{
    /**
     * @Route("/", name="comic_index", methods={"GET"})
     */
    public function index(ComicRepository $comicRepository): Response
    {
        return $this->render('admin/comic/index.html.twig', [
            'comics' => $comicRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="comic_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $comic = new Comic();
        $form = $this->createForm(ComicType::class, $comic);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comic);
            $entityManager->flush();

            return $this->redirectToRoute('comic_index');
        }

        return $this->render('admin/comic/new.html.twig', [
            'comic' => $comic,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="comic_show", methods={"GET"})
     */
    public function show(Comic $comic): Response
    {
        return $this->render('admin/comic/show.html.twig', [
            'comic' => $comic,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="comic_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Comic $comic): Response
    {
        $form = $this->createForm(ComicType::class, $comic);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('comic_index');
        }

        return $this->render('admin/comic/edit.html.twig', [
            'comic' => $comic,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="comic_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Comic $comic): Response
    {
        if ($this->isCsrfTokenValid('delete' . $comic->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($comic);
            $entityManager->flush();
        }

        return $this->redirectToRoute('comic_index');
    }
}
