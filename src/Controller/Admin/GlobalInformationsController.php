<?php

namespace App\Controller\Admin;

use App\Entity\GlobalInformations;
use App\Form\GlobalInformationsType;
use App\Repository\GlobalInformationsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/global/informations")
 */
class GlobalInformationsController extends AbstractController
{
    /**
     * @Route("/", name="global_informations_index", methods={"GET"})
     */
    public function index(GlobalInformationsRepository $globalInformationsRepository): Response
    {
        $globalInfo = $globalInformationsRepository->findAll();
        // dd($globalInfo);
        if ($globalInformationsRepository === null) {
            return $this->redirectToRoute('global_informations_new');
        } else {

            return $this->redirectToRoute('global_informations_edit', ['id' => 1]);
        };
    }

    /**
     * @Route("/new", name="global_informations_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $globalInformation = new GlobalInformations();
        $form = $this->createForm(GlobalInformationsType::class, $globalInformation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($globalInformation);
            $entityManager->flush();

            return $this->redirectToRoute('global_informations_index');
        }

        return $this->render('admin/global_informations/new.html.twig', [
            'global_information' => $globalInformation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="global_informations_show", methods={"GET"})
     */
    public function show(GlobalInformations $globalInformation): Response
    {
        return $this->render('admin/global_informations/show.html.twig', [
            'global_information' => $globalInformation,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="global_informations_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, GlobalInformations $globalInformation): Response
    {
        $form = $this->createForm(GlobalInformationsType::class, $globalInformation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('global_informations_index');
        }

        return $this->render('admin/global_informations/edit.html.twig', [
            'global_information' => $globalInformation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="global_informations_delete", methods={"DELETE"})
     */
    public function delete(Request $request, GlobalInformations $globalInformation): Response
    {
        if ($this->isCsrfTokenValid('delete' . $globalInformation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($globalInformation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('global_informations_index');
    }
}
