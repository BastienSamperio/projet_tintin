<?php

namespace App\Controller\Admin;

use App\Entity\EmblematicPlaces;
use App\Form\EmblematicPlacesType;
use App\Repository\EmblematicPlacesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/emblematicPlaces")
 */
class EmblematicPlacesController extends AbstractController
{
    /**
     * @Route("/", name="emblematic_places_index", methods={"GET"})
     */
    public function index(EmblematicPlacesRepository $emblematicPlacesRepository): Response
    {
        return $this->render('admin/emblematic_places/index.html.twig', [
            'emblematic_places' => $emblematicPlacesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="emblematic_places_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $emblematicPlace = new EmblematicPlaces();
        $form = $this->createForm(EmblematicPlacesType::class, $emblematicPlace);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($emblematicPlace);
            $entityManager->flush();

            return $this->redirectToRoute('emblematic_places_index');
        }

        return $this->render('admin/emblematic_places/new.html.twig', [
            'emblematic_place' => $emblematicPlace,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="emblematic_places_show", methods={"GET"})
     */
    public function show(EmblematicPlaces $emblematicPlace): Response
    {
        return $this->render('admin/emblematic_places/show.html.twig', [
            'emblematic_place' => $emblematicPlace,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="emblematic_places_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EmblematicPlaces $emblematicPlace): Response
    {
        $form = $this->createForm(EmblematicPlacesType::class, $emblematicPlace);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('emblematic_places_index');
        }

        return $this->render('admin/emblematic_places/edit.html.twig', [
            'emblematic_place' => $emblematicPlace,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="emblematic_places_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EmblematicPlaces $emblematicPlace): Response
    {
        if ($this->isCsrfTokenValid('delete' . $emblematicPlace->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($emblematicPlace);
            $entityManager->flush();
        }

        return $this->redirectToRoute('emblematic_places_index');
    }
}
