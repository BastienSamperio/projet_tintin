<?php

namespace App\Controller;

use App\Entity\Character;
use App\Entity\EmblematicPlaces;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class WikiController extends AbstractController
{
    /**
     * @Route("/wiki/character/{FirstName}", name="characterWiki")
     */
    public function characterWiki(Character $character)
    {
        return $this->render('public/wikiBaseCharacter.html.twig', [
            'character' => $character,
        ]);
    }
    /**
     * @Route("/wiki/places/{Name}", name="placeWiki")
     */
    public function placeWiki(EmblematicPlaces $EmblematicPlaces)
    {
        return $this->render('public/wikiBasePlaces.html.twig', [
            'EmblematicPlaces' => $EmblematicPlaces,
        ]);
    }
}
