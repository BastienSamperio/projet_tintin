<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CharacterRepository;
use App\Repository\EmblematicPlacesRepository;

class PublicController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index()
    {
        return $this->render('public/index.html.twig', [
            'controller_name' => 'PublicController',
        ]);
    }
    /**
     * @Route("/wiki", name="accueilWiki")
     */
    public function indexWiki(CharacterRepository $characterRepository, EmblematicPlacesRepository $emblematicPlacesRepository)
    {
        return $this->render('public/indexWiki.html.twig', ['characters' => $characterRepository->findAll(), 'emblematic_places' => $emblematicPlacesRepository->findAll()]);
    }
    /**
     * @Route("/game", name="game")
     */
    public function game()
    {
        return $this->render('public/game.html.twig');
    }
}
