<?php

namespace App\Repository;

use App\Entity\Cartoon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Cartoon|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cartoon|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cartoon[]    findAll()
 * @method Cartoon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CartoonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cartoon::class);
    }

    // /**
    //  * @return Cartoon[] Returns an array of Cartoon objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Cartoon
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
