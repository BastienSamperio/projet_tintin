<?php

namespace App\Repository;

use App\Entity\EmblematicPlaces;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EmblematicPlaces|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmblematicPlaces|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmblematicPlaces[]    findAll()
 * @method EmblematicPlaces[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmblematicPlacesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmblematicPlaces::class);
    }

    // /**
    //  * @return EmblematicPlaces[] Returns an array of EmblematicPlaces objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EmblematicPlaces
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
