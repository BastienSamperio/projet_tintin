<?php

namespace App\Entity;

use App\Repository\GlobalInformationsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass=GlobalInformationsRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class GlobalInformations
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     */
    private $filenameFavicon;

    /**
     * @var File|null
     * @Vich\UploadableField(mapping="global_ico", fileNameProperty="filenameFavicon")
     */
    private $imageFileFavicon;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     */
    private $filenameGlobalImage;

    /**
     * @var File|null
     * @Vich\UploadableField(mapping="global_image", fileNameProperty="filenameGlobalImage")
     */
    private $imageFileGlobalImage;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $PrimaryMenu1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $PrimaryMenu2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $PrimaryMenu3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $PrimaryMenu4;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /*
     * @return null|string
     */
    public function getFilenameFavicon(): ?string
    {
        return $this->filenameFavicon;
    }

    /**
     * @param null|string $filenameFavicon
     * @return GlobalInformations
     */
    public function setFilenameFavicon(?string $filenameFavicon): GlobalInformations
    {
        $this->filenameFavicon = $filenameFavicon;
        return $this;
    }

    /**
     * @return null|File
     */
    public function getImageFileFavicon(): ?File
    {
        return $this->imageFileFavicon;
    }

    /**
     * @param null|File $imageFileFavicon
     * @return GlobalInformations
     */
    public function setImageFileFavicon(?File $imageFileFavicon): GlobalInformations
    {
        $this->imageFileFavicon = $imageFileFavicon;
        if ($this->imageFileFavicon instanceof UploadedFile) {
            $this->updated_at = new \DateTime('now');
        }
        return $this;
    }

    /*
     * @return null|string
     */
    public function getFilenameGlobalImage(): ?string
    {
        return $this->filenameGlobalImage;
    }

    /**
     * @param null|string $filenameGlobalImage
     * @return GlobalInformations
     */
    public function setFilenameGlobalImage(?string $filenameGlobalImage): GlobalInformations
    {
        $this->filenameGlobalImage = $filenameGlobalImage;
        return $this;
    }

    /**
     * @return null|File
     */
    public function getImageFileGlobalImage(): ?File
    {
        return $this->imageFileGlobalImage;
    }

    /**
     * @param null|File $imageFileGlobalImage
     * @return GlobalInformations
     */
    public function setImageFileGlobalImage(?File $imageFileGlobalImage): GlobalInformations
    {
        $this->imageFileGlobalImage = $imageFileGlobalImage;
        if ($this->imageFileGlobalImage instanceof UploadedFile) {
            $this->updated_at = new \DateTime('now');
        }
        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAtValue()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setUpdatedAtValue()
    {
        $this->updated_at = new \DateTime();
    }

    public function getPrimaryMenu1(): ?string
    {
        return $this->PrimaryMenu1;
    }

    public function setPrimaryMenu1(?string $PrimaryMenu1): self
    {
        $this->PrimaryMenu1 = $PrimaryMenu1;

        return $this;
    }

    public function getPrimaryMenu2(): ?string
    {
        return $this->PrimaryMenu2;
    }

    public function setPrimaryMenu2(?string $PrimaryMenu2): self
    {
        $this->PrimaryMenu2 = $PrimaryMenu2;

        return $this;
    }

    public function getPrimaryMenu3(): ?string
    {
        return $this->PrimaryMenu3;
    }

    public function setPrimaryMenu3(?string $PrimaryMenu3): self
    {
        $this->PrimaryMenu3 = $PrimaryMenu3;

        return $this;
    }

    public function getPrimaryMenu4(): ?string
    {
        return $this->PrimaryMenu4;
    }

    public function setPrimaryMenu4(?string $PrimaryMenu4): self
    {
        $this->PrimaryMenu4 = $PrimaryMenu4;

        return $this;
    }
}
