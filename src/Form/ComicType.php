<?php

namespace App\Form;

use App\Entity\Character;
use App\Entity\Comic;
use App\Entity\EmblematicPlaces;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ComicType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imageFile', VichImageType::class, [
                'required' => false
            ])
            ->add('Name')
            ->add('Year')
            ->add('content', CKEditorType::class, array('config' => array('uiColor' => '#ffffff')))
            ->add('Characters', EntityType::class, [
                'class' => Character::class,
                'choice_label' => 'LastName',
                'multiple' => 'true',
                'expanded' => 'true',
            ])
            ->add('Places', EntityType::class, [
                'class' => EmblematicPlaces::class,
                'choice_label' => 'Name',
                'multiple' => 'true',
                'expanded' => 'true',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comic::class,
        ]);
    }
}
