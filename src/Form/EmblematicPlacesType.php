<?php

namespace App\Form;

use App\Entity\Cartoon;
use App\Entity\Character;
use App\Entity\Comic;
use App\Entity\EmblematicPlaces;
use App\Entity\Game;
use App\Entity\Movie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EmblematicPlacesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imageFile', VichImageType::class, [
                'required' => false
            ])
            ->add('Name')
            ->add('Country')
            ->add('content', CKEditorType::class, array('config' => array('uiColor' => '#ffffff')))
            ->add('Comics', EntityType::class, [
                'class' => Comic::class,
                'choice_label' => 'Name',
                'multiple' => 'true',
                'expanded' => 'true',
            ])
            ->add('Movies', EntityType::class, [
                'class' => Movie::class,
                'choice_label' => 'Name',
                'multiple' => 'true',
                'expanded' => 'true',
            ])
            ->add('Games', EntityType::class, [
                'class' => Game::class,
                'choice_label' => 'Name',
                'multiple' => 'true',
                'expanded' => 'true',
            ])
            ->add('Cartoons', EntityType::class, [
                'class' => Cartoon::class,
                'choice_label' => 'Name',
                'multiple' => 'true',
                'expanded' => 'true',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EmblematicPlaces::class,
        ]);
    }
}
