<?php

namespace App\Form;

use App\Entity\Cartoon;
use App\Entity\Character;
use App\Entity\Comic;
use App\Entity\Game;
use App\Entity\Movie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CharacterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imageFile', VichImageType::class, [
                'required' => false
            ])
            ->add('FirstName', TextType::class, ['label' => 'Prénom'])
            ->add('LastName', TextType::class, ['label' => 'Nom de famille'])
            ->add('FirstApparition', NumberType::class, ['label' => 'Première apparition'])
            ->add('content', CKEditorType::class, array('config' => array('uiColor' => '#ffffff'), 'label' => "Contenu"))
            ->add('Comics', EntityType::class, [
                'class' => Comic::class,
                'choice_label' => 'Name',
                'multiple' => 'true',
                'expanded' => 'true',
                'label' => 'Bande Dessinés'
            ])
            ->add('Movies', EntityType::class, [
                'class' => Movie::class,
                'choice_label' => 'Name',
                'multiple' => 'true',
                'expanded' => 'true',
                'label' => 'Films'
            ])
            ->add('Games', EntityType::class, [
                'class' => Game::class,
                'choice_label' => 'Name',
                'multiple' => 'true',
                'expanded' => 'true',
                'label' => 'Jeux Vidéos'
            ])
            ->add('Cartoons', EntityType::class, [
                'class' => Cartoon::class,
                'choice_label' => 'Name',
                'multiple' => 'true',
                'expanded' => 'true',
                'label' => 'Dessins Animés'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Character::class,
        ]);
    }
}
