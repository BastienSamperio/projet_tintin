<?php

namespace App\Form;

use App\Entity\GlobalInformations;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GlobalInformations1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('filenameFavicon')
            ->add('filenameGlobalImage')
            ->add('created_at')
            ->add('updated_at')
            ->add('PrimaryMenu1')
            ->add('PrimaryMenu2')
            ->add('PrimaryMenu3')
            ->add('PrimaryMenu4')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GlobalInformations::class,
        ]);
    }
}
