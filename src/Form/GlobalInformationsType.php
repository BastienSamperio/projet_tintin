<?php

namespace App\Form;

use App\Entity\GlobalInformations;
use App\Entity\Page;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class GlobalInformationsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imageFileFavicon', VichImageType::class, ['required' => false])
            ->add('imageFileGlobalImage', VichImageType::class, ['required' => false])
            // ->add('created_at')
            // ->add('updated_at')
            // ->add('PrimaryMenu1', EntityType::class, [
            //     'class' => Page::class,
            //     'choice_label' => 'Name',
            //     'required'   => false,
            //     'empty_data' => 'Choissisez une page',
            // ])
            // ->add('PrimaryMenu2', EntityType::class, [
            //     'class' => Page::class,
            //     'choice_label' => 'Name',
            //     'required'   => false,
            //     'empty_data' => 'Choissisez une page',
            // ])
            // ->add('PrimaryMenu3', EntityType::class, [
            //     'class' => Page::class,
            //     'choice_label' => 'Name',
            //     'required'   => false,
            //     'empty_data' => 'Choissisez une page',
            // ])
            // ->add('PrimaryMenu4', EntityType::class, [
            //     'class' => Page::class,
            //     'choice_label' => 'Name',
            //     'required'   => false,
            //     'empty_data' => 'Choissisez une page',
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GlobalInformations::class,
        ]);
    }
}
