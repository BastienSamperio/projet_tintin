//on charge le DOMcontent afin de pouvoir utiliser les "document.createElement || element.appendChild(element2) etc..."
document.addEventListener("DOMContentLoaded", function () {

    let gameHasStarted = false; //variable pour savoir si le jeu a commencé || DO NOT CHANGE

    //on récupère les eléments principaux du HTML
    let body = document.querySelector("body");
    let bg = document.getElementById('bg');
    let timer = document.getElementById('timer');

    //le message des 4.50min reste invisible jusque au bon moment
    timer.style.opacity = 0;




    // ROTATE DEVICE
    //On check l'orientation du device pour savoir si on affiche l'alerte (reste en CSS)
    let wheight = window.innerHeight;
    let wwidth = window.innerWidth;

    let overlay = document.getElementById("overlay");
    let iconContainer = document.getElementById("iconContainer");


    if (wheight > wwidth) {
        overlay.classList.add("rotate_index");
        iconContainer.classList.add("rotate_index");
        bg.style.width = ('200%');
    } else {
        overlay.classList.remove("rotate_index");
        iconContainer.classList.remove("rotate_index");
    }

    window.addEventListener("resize", function (event) {
        let wheight = window.innerHeight;
        let wwidth = window.innerWidth;

        let overlay = document.getElementById("overlay");
        let iconContainer = document.getElementById("iconContainer");


        if (wheight > wwidth) {
            overlay.classList.add("rotate_index");
            iconContainer.classList.add("rotate_index");
            bg.style.width = ('200%');
        } else {
            overlay.classList.remove("rotate_index");
            iconContainer.classList.remove("rotate_index");
        }
    });

    //AideMenuAlex
    //On créer et remplis le menu d'explication du jeu
    if (wheight < wwidth) {

        let dark_bg = document.createElement('div');
        let aide_container = document.createElement('div');
        let aide_header = document.createElement('div');
        let close_cross = document.createElement('i');
        let aide_footer = document.createElement('div');
        let aide_body = document.createElement('div');
        let aide_title = document.createElement('div');
        let aide_content = document.createElement('div');
        let aide_button = document.createElement('button');

        let aide_title_txt = document.createTextNode("Tintin et le chateau de Moulinsart");
        let aide_content_txt = document.createTextNode("Tintin vient de retrouver le sceptre d'Ottokar, celui-ci avait été dérobbé par le terrible général Alcazar, vous devez aller vous réfugier dans le chateau de moulinsart avant que ce dernier ne vous attrape, pour cela, évitez les obstacles et semez Alcazar, les gardiens du sceptre vous attendent dans le chateau, ne les décevez pas... Pour jouer il vous suffit de cliquer n'importe ou sur la page afin de faire sauter tintin, évitez les objets au sol et les oiseaux, si vous heurtez un obstacle vous serez ralenti et le général vous rattrapera !!!");
        let aide_button_txt = document.createTextNode("Prendre le sceptre");

        body.appendChild(dark_bg);
        body.appendChild(aide_container);
        aide_container.appendChild(aide_header);
        aide_container.appendChild(aide_body);
        aide_container.appendChild(aide_footer);
        aide_header.appendChild(aide_title);
        aide_title.appendChild(aide_title_txt);
        aide_header.appendChild(close_cross);
        aide_body.appendChild(aide_content);
        aide_content.appendChild(aide_content_txt);
        aide_footer.appendChild(aide_button);
        aide_button.appendChild(aide_button_txt);


        aide_header.id = "aide_header";
        close_cross.classList.add("close_cross");
        close_cross.classList.add("fas");
        close_cross.classList.add("fa-times");
        close_cross.classList.add("fa_aide");
        close_cross.setAttribute('title', 'Quitter le jeu');
        close_cross.setAttribute('href', '/')
        aide_title.classList.add('aide_title');


        aide_body.id = "aide_body";
        aide_content.classList.add('aide_content');

        aide_footer.id = "aide_footer";
        aide_button.classList.add('aide_button');
        aide_button.id = 'aide_button';

        dark_bg.id = "dark_bg";
        dark_bg.style.height = wheight;
        dark_bg.style.width = wwidth;
        aide_container.id = "aide_container";
        aide_container.style.height = wheight * 0.6;
        aide_container.style.width = wwidth * 0.4;


        close_cross.addEventListener("click", () => {
            aide_container.style.top = "5%";
            aide_container.style.opacity = "0";
            aide_container.style.transition = "0.16s";
            dark_bg.style.opacity = "0";
            dark_bg.style.transition = "0.16s";
            setTimeout(function () {
                window.location.href = "/";
                dark_bg.classList.add("menu_hide");
                aide_container.classList.add("menu_hide");
            }, 150);
        });

        aide_button.addEventListener("click", () => {
            aide_container.style.top = "5%";
            aide_container.style.opacity = "0";
            aide_container.style.transition = "0.16s";
            dark_bg.style.opacity = "0";
            dark_bg.style.transition = "0.16s";
            setTimeout(function () {
                dark_bg.classList.add("menu_hide");
                aide_container.classList.add("menu_hide");
            }, 150);
        });
    }
    //AideMenuAlexFin

    let aide_button = document.getElementById('aide_button')
    let musicButton = document.createElement('div');
    let musicItem = document.createElement('i');

    musicButton.appendChild(musicItem);
    body.appendChild(musicButton);


    //On lance le jeu
    aide_button.addEventListener("click", () => {
        gameHasStarted = true

        if (gameHasStarted == true) {
            //musique
            musicButton.classList.add('musicButton');
            musicItem.classList.add('fas');
            musicItem.classList.add('fa-volume-up');
            var EnableMusic = true;
            var audio = new Audio('../Sound/Tintin_Générique.mp3');
            audio.play();
            function PlayMusique() {
                return setInterval(function () {
                    if (EnableMusic == true) {
                        audio.play();
                    }
                }, 0);
            };

            var id = PlayMusique();

            setTimeout(function () {
                clearInterval(id);
            }, 300000);

            musicButton.addEventListener("click", () => {
                if (EnableMusic == true) {
                    musicItem.classList.remove('fa-volume-up');
                    musicItem.classList.add('fa-volume-mute');
                    EnableMusic = false;
                    audio.pause();
                } else {
                    musicItem.classList.remove('fa-volume-mute');
                    musicItem.classList.add('fa-volume-up');
                    EnableMusic = true;
                    audio.play();
                }
            });


            //décallage du BG
            let wheight = window.innerHeight;
            let wwidth = window.innerWidth;
            if (wheight > wwidth) {
                interval = 1200;
            } else {
                interval = 600;
            }
            var newpos = 0;

            function BGmove() {
                return setInterval(function () {
                    bg.style.left = newpos;
                    newpos = newpos - 1;
                }, interval);
            };

            var id = BGmove();

            setTimeout(function () {
                clearInterval(id);
            }, 300000);


            setTimeout(() => {
                timer_opac = true;
                function timerAltern() {
                    return setInterval(function () {
                        if (timer_opac == false) {
                            timer.style.opacity = 0.3;
                            timer_opac = true;
                        } else {
                            timer.style.opacity = 1;
                            timer_opac = false;
                        }
                    }, 500);
                };
                var id = timerAltern();

                setTimeout(function () {
                    clearInterval(id);
                    timer.style.opacity = 0;
                }, 10000);
            }, 240000);


            //Course des personnages
            let tintin = document.createElement('img');
            let alcazar = document.createElement('img');

            body.appendChild(tintin);
            body.appendChild(alcazar);

            tintin.id = 'tintin';
            tintin.style.position = 'fixed';
            tintin.style.bottom = '2%';
            tintin.style.left = '45%';
            tintin.setAttribute('src', '/images/gameTintin/Tintincourt1.png');
            tintin.style.height = '30%';

            let pos1 = true
            let pos2 = false
            function AlternTitnin() {
                return setInterval(function () {
                    if (tintin.style.bottom <= '4%') {
                        if (pos1 == true) {
                            tintin.removeAttribute('src', '/images/gameTintin/Tintincourt2.png');
                            tintin.setAttribute('src', '/images/gameTintin/Tintincourt1.png');
                            pos1 = false
                            pos2 = true
                        } else {
                            tintin.removeAttribute('src', '/images/gameTintin/Tintincourt1.png');
                            tintin.setAttribute('src', '/images/gameTintin/Tintincourt2.png');
                            pos2 = false
                            pos1 = true
                        }
                    }
                }, 250);
            };

            var id = AlternTitnin();

            setTimeout(function () {
                clearInterval(id);
            }, 300000);

            alcazar.id = 'alcazar';
            alcazar.style.position = 'fixed';
            alcazar.style.bottom = '0';
            alcazar.setAttribute('src', '/images/gameTintin/alcazar.png');
            alcazar.style.height = '35%';
            alcazar.style.left = '20%';

            let pos3 = true
            let pos4 = false
            let posx = alcazar.offsetLeft;
            let difficultyValue = 400; //Higher numbers = more difficulty ||  Default = 400 ||  WARNING less would make the player win with Alcazar still on the screen, if you change that change "difficulty_decal" accordingly
            let difficulty_decal = 1; //Touche only if you are changing the "difficultyValue" value
            let posLose = alcazar.offsetLeft + alcazar.offsetWidth + difficultyValue;
            function AlternAlcazar() {
                return setInterval(function () {
                    if (pos3 == true) {
                        alcazar.removeAttribute('src', '/images/gameTintin/alcazar2.png');
                        alcazar.setAttribute('src', '/images/gameTintin/alcazar.png');
                        alcazar.style.left = posx + 30;
                        posx = posx - difficulty_decal;
                        posLose = posLose - 1;
                        pos3 = false
                        pos4 = true

                    } else {
                        alcazar.removeAttribute('src', '/images/gameTintin/alcazar.png');
                        alcazar.setAttribute('src', '/images/gameTintin/alcazar2.png');
                        posx = posx - difficulty_decal;
                        posLose = posLose - 1;
                        pos4 = false
                        pos3 = true
                    }
                }, 280);
            };



            var id = AlternAlcazar();

            setTimeout(function () {
                clearInterval(id);
            }, 300000);

            //Sauts de Tintin
            body.addEventListener("mousedown", () => {
                let tintin = document.getElementById('tintin');
                tintin.style.bottom = '20%';
                tintin.style.transition = '0.02s';
                tintin.setAttribute('src', '/images/gameTintin/Tintincourt3.png');
                setTimeout(function () {
                    tintin.style.bottom = '2%';
                }, 700);
                setTimeout(function () {
                    tintin.removeAttribute('src', '/images/gameTintin/Tintincourt3.png');
                    tintin.setAttribute('src', '/images/gameTintin/Tintincourt1.png');
                }, 200);
            });
            body.addEventListener("mouseup", () => {
                let tintin = document.getElementById('tintin');
                tintin.style.bottom = '2%';
                tintin.removeAttribute('src', '/images/gameTintin/Tintincourt3.png');
                tintin.setAttribute('src', '/images/gameTintin/Tintincourt1.png');
            });


            //Apparition, mouvement des Objets || Si tintin touche un objet Alcazar le ratrappe et il clignotte
            function random(min, max) {
                return ~~(Math.random() * (max - min + 1) + min);
            }
            let Object1 = document.createElement('img');
            let Object2 = document.createElement('img');
            let Object3 = document.createElement('img');
            let Object4 = document.createElement('img');
            let Object5 = document.createElement('img');
            let Object6 = document.createElement('img');
            let ObjectBird = document.createElement('img');

            body.appendChild(Object1);
            body.appendChild(Object2);
            body.appendChild(Object3);
            body.appendChild(Object4);
            body.appendChild(Object5);
            body.appendChild(Object6);
            body.appendChild(ObjectBird);

            Object1.classList.add('object');
            Object2.classList.add('object');
            Object3.classList.add('object');
            Object4.classList.add('object');
            Object5.classList.add('object');
            Object6.classList.add('object');
            ObjectBird.classList.add('object');




            function SpawnObject1() {
                timeRandom = random(4000, 6000);
                return setInterval(function () {
                    spriteRandom = random(1, 7);
                    if (spriteRandom == 1) {
                        Object1.setAttribute('src', '/images/gameTintin/chapeau.png')
                    }
                    if (spriteRandom == 2) {
                        Object1.setAttribute('src', '/images/gameTintin/coffre.png')
                    }
                    if (spriteRandom == 3) {
                        Object1.setAttribute('src', '/images/gameTintin/licorne.png')
                    }
                    if (spriteRandom == 4) {
                        Object1.setAttribute('src', './images/gameTintin/milou.png')
                    }
                    if (spriteRandom == 5) {
                        Object1.setAttribute('src', '/images/gameTintin/nainjardin.png')
                    }
                    if (spriteRandom == 6) {
                        Object1.setAttribute('src', '/images/gameTintin/pirogue.png')
                    }
                    if (spriteRandom == 7) {
                        Object1.setAttribute('src', '/images/gameTintin/vase.png')
                    }
                    Object1.style.right = -400;
                    function MoveObject1() {
                        posObj1 = -400;
                        return setInterval(function () {
                            Object1.style.right = posObj1;
                            posObj1 = posObj1 + 4;
                        }, 1);
                    };
                    var id = MoveObject1();

                    setTimeout(function () {
                        clearInterval(id);
                    }, timeRandom);
                }, timeRandom);
            };
            var id = SpawnObject1();

            setTimeout(function () {
                clearInterval(id);
            }, 300000);

            function Touche1() {
                return setInterval(function () {
                    tintin1Bottom = tintin.offsetTop + tintin.offsetHeight;
                    Object1Top = Object1.offsetTop;
                    tintin1Front = tintin.offsetLeft + tintin.offsetWidth - 50;
                    alcazar1Front = alcazar.offsetLeft;
                    tintin1Back = tintin.offsetLeft + 100;
                    alcazar1Back = alcazar.offsetLeft;
                    Object1Front = Object1.offsetLeft;
                    Object1Back = Object1.offsetLeft + Object1.offsetWidth;

                    if (tintin1Bottom > Object1Top & tintin1Front > Object1Front & tintin1Back < Object1Back) {
                        function getHurt() {
                            let tintinHurt = false
                            return setInterval(function () {
                                if (tintinHurt == false) {
                                    tintinHurt = true
                                    tintin.style.opacity = "0.5";
                                } else {
                                    tintinHurt = false
                                    tintin.style.opacity = "1";
                                }
                            }, 350);
                        };

                        var id = getHurt();

                        setTimeout(function () {
                            clearInterval(id);
                            tintin.style.opacity = "1";
                            posx = posx + 5;
                            posLose = posLose + 5;
                        }, 2000);
                    }
                }, 100);

            };

            var id = Touche1();

            setTimeout(function () {
                clearInterval(id);
            }, 300000);

            setTimeout(function () {
                function SpawnObject2() {
                    timeRandom2 = random(4000, 6000);
                    return setInterval(function () {
                        spriteRandom2 = random(1, 6);
                        if (spriteRandom2 == 1) {
                            Object2.setAttribute('src', '/images/gameTintin/chapeau.png')
                        }
                        if (spriteRandom2 == 2) {
                            Object2.setAttribute('src', '/images/gameTintin/coffre.png')
                        }
                        if (spriteRandom2 == 3) {
                            Object2.setAttribute('src', '/images/gameTintin/licorne.png')
                        }
                        if (spriteRandom2 == 4) {
                            Object2.setAttribute('src', '/images/gameTintin/nainjardin.png')
                        }
                        if (spriteRandom2 == 5) {
                            Object2.setAttribute('src', '/images/gameTintin/pirogue.png')
                        }
                        if (spriteRandom2 == 6) {
                            Object2.setAttribute('src', '/images/gameTintin/vase.png')
                        }
                        Object2.style.right = -400;
                        function MoveObject2() {
                            posObj2 = -400;
                            return setInterval(function () {
                                Object2.style.right = posObj2;
                                posObj2 = posObj2 + 4;
                            }, 1);
                        };
                        var id = MoveObject2();

                        setTimeout(function () {
                            clearInterval(id);
                        }, timeRandom2);
                    }, timeRandom2);
                };
                var id = SpawnObject2();

                setTimeout(function () {
                    clearInterval(id);
                }, 300000);

                function Touche2() {
                    return setInterval(function () {
                        tintin2Bottom = tintin.offsetTop + tintin.offsetHeight;
                        Object2Top = Object2.offsetTop;
                        tintin2Front = tintin.offsetLeft + tintin.offsetWidth - 50;
                        alcazar2Front = alcazar.offsetLeft;
                        tintin2Back = tintin.offsetLeft + 100;
                        alcazar2Back = alcazar.offsetLeft;
                        Object2Front = Object2.offsetLeft;
                        Object2Back = Object2.offsetLeft + Object2.offsetWidth;

                        if (tintin2Bottom > Object2Top & tintin2Front > Object2Front & tintin2Back < Object2Back) {
                            function getHurt() {
                                let tintinHurt = false
                                return setInterval(function () {
                                    if (tintinHurt == false) {
                                        tintinHurt = true
                                        tintin.style.opacity = "0.5";
                                    } else {
                                        tintinHurt = false
                                        tintin.style.opacity = "1";
                                    }
                                }, 350);
                            };

                            var id = getHurt();

                            setTimeout(function () {
                                clearInterval(id);
                                tintin.style.opacity = "1";
                                posx = posx + 5;
                                posLose = posLose + 5;
                            }, 2000);
                        }
                    }, 100);

                };



                var id = Touche2();

                setTimeout(function () {
                    clearInterval(id);
                }, 300000);
            }, 3000);

            setTimeout(function () {
                function SpawnObject3() {
                    timeRandom3 = random(4000, 6000);
                    return setInterval(function () {
                        spriteRandom3 = random(1, 6);
                        if (spriteRandom3 == 1) {
                            Object3.setAttribute('src', '/images/gameTintin/chapeau.png')
                        }
                        if (spriteRandom3 == 2) {
                            Object3.setAttribute('src', '/images/gameTintin/coffre.png')
                        }
                        if (spriteRandom3 == 3) {
                            Object3.setAttribute('src', '/images/gameTintin/licorne.png')
                        }
                        if (spriteRandom3 == 4) {
                            Object3.setAttribute('src', '/images/gameTintin/nainjardin.png')
                        }
                        if (spriteRandom3 == 5) {
                            Object3.setAttribute('src', '/images/gameTintin/pirogue.png')
                        }
                        if (spriteRandom3 == 6) {
                            Object3.setAttribute('src', '/images/gameTintin/vase.png')
                        }
                        Object3.style.right = -400;
                        function MoveObject3() {
                            posObj3 = -400;
                            return setInterval(function () {
                                Object3.style.right = posObj3;
                                posObj3 = posObj3 + 4;
                            }, 1);
                        };
                        var id = MoveObject3();

                        setTimeout(function () {
                            clearInterval(id);
                        }, timeRandom3);
                    }, timeRandom3);
                };
                var id = SpawnObject3();

                setTimeout(function () {
                    clearInterval(id);
                }, 300000);

                function Touche3() {
                    return setInterval(function () {
                        tintin3Bottom = tintin.offsetTop + tintin.offsetHeight;
                        Object3Top = Object3.offsetTop;
                        tintin3Front = tintin.offsetLeft + tintin.offsetWidth - 50;
                        alcazar3Front = alcazar.offsetLeft;
                        tintin3Back = tintin.offsetLeft + 100;
                        alcazar3Back = alcazar.offsetLeft;
                        Object3Front = Object3.offsetLeft;
                        Object3Back = Object3.offsetLeft + Object3.offsetWidth;

                        if (tintin3Bottom > Object3Top & tintin3Front > Object3Front & tintin3Back < Object3Back) {
                            function getHurt() {
                                let tintinHurt = false
                                return setInterval(function () {
                                    if (tintinHurt == false) {
                                        tintinHurt = true
                                        tintin.style.opacity = "0.5";
                                    } else {
                                        tintinHurt = false
                                        tintin.style.opacity = "1";
                                    }
                                }, 350);
                            };

                            var id = getHurt();

                            setTimeout(function () {
                                clearInterval(id);
                                tintin.style.opacity = "1";
                                posx = posx + 5;
                                posLose = posLose + 5;
                            }, 2000);
                        }
                    }, 100);

                };



                var id = Touche3();

                setTimeout(function () {
                    clearInterval(id);
                }, 300000);
            }, 6000);

            setTimeout(function () {
                function SpawnObject4() {
                    timeRandom4 = random(4000, 6000);
                    return setInterval(function () {
                        spriteRandom4 = random(1, 5);
                        if (spriteRandom4 == 1) {
                            Object4.setAttribute('src', '/images/gameTintin/coffre.png')
                        }
                        if (spriteRandom4 == 2) {
                            Object4.setAttribute('src', '/images/gameTintin/licorne.png')
                        }
                        if (spriteRandom4 == 3) {
                            Object4.setAttribute('src', '/images/gameTintin/nainjardin.png')
                        }
                        if (spriteRandom4 == 4) {
                            Object4.setAttribute('src', '/images/gameTintin/pirogue.png')
                        }
                        if (spriteRandom4 == 5) {
                            Object4.setAttribute('src', '/images/gameTintin/vase.png')
                        }
                        Object4.style.right = -400;
                        function MoveObject4() {
                            posObj4 = -400;
                            return setInterval(function () {
                                Object4.style.right = posObj4;
                                posObj4 = posObj4 + 4;
                            }, 1);
                        };
                        var id = MoveObject4();

                        setTimeout(function () {
                            clearInterval(id);
                        }, timeRandom4);
                    }, timeRandom4);
                };
                var id = SpawnObject4();

                setTimeout(function () {
                    clearInterval(id);
                }, 300000);

                function Touche4() {
                    return setInterval(function () {
                        tintin4Bottom = tintin.offsetTop + tintin.offsetHeight;
                        Object4Top = Object4.offsetTop;
                        tintin4Front = tintin.offsetLeft + tintin.offsetWidth - 50;
                        alcazar4Front = alcazar.offsetLeft;
                        tintin4Back = tintin.offsetLeft + 100;
                        alcazar4Back = alcazar.offsetLeft;
                        Object4Front = Object4.offsetLeft;
                        Object4Back = Object4.offsetLeft + Object4.offsetWidth;

                        if (tintin4Bottom > Object4Top & tintin4Front > Object4Front & tintin4Back < Object4Back) {
                            function getHurt() {
                                let tintinHurt = false
                                return setInterval(function () {
                                    if (tintinHurt == false) {
                                        tintinHurt = true
                                        tintin.style.opacity = "0.5";
                                    } else {
                                        tintinHurt = false
                                        tintin.style.opacity = "1";
                                    }
                                }, 350);
                            };

                            var id = getHurt();

                            setTimeout(function () {
                                clearInterval(id);
                                tintin.style.opacity = "1";
                                posx = posx + 5;
                                posLose = posLose + 5;
                            }, 2000);
                        }
                    }, 100);

                };



                var id = Touche4();

                setTimeout(function () {
                    clearInterval(id);
                }, 300000);
            }, 9000);

            setTimeout(function () {
                function SpawnObject5() {
                    timeRandom5 = random(4000, 6000);
                    return setInterval(function () {
                        spriteRandom5 = random(1, 5);
                        if (spriteRandom5 == 1) {
                            Object5.setAttribute('src', '/images/gameTintin/coffre.png')
                        }
                        if (spriteRandom5 == 2) {
                            Object5.setAttribute('src', '/images/gameTintin/licorne.png')
                        }
                        if (spriteRandom5 == 3) {
                            Object5.setAttribute('src', '/images/gameTintin/nainjardin.png')
                        }
                        if (spriteRandom5 == 4) {
                            Object5.setAttribute('src', '/images/gameTintin/pirogue.png')
                        }
                        if (spriteRandom5 == 5) {
                            Object5.setAttribute('src', '/images/gameTintin/vase.png')
                        }
                        Object5.style.right = -400;
                        function MoveObject5() {
                            posObj5 = -400;
                            return setInterval(function () {
                                Object5.style.right = posObj5;
                                posObj5 = posObj5 + 4;
                            }, 1);
                        };
                        var id = MoveObject5();

                        setTimeout(function () {
                            clearInterval(id);
                        }, timeRandom5);
                    }, timeRandom5);
                };
                var id = SpawnObject5();

                setTimeout(function () {
                    clearInterval(id);
                }, 300000);

                function Touche5() {
                    return setInterval(function () {
                        tintin5Bottom = tintin.offsetTop + tintin.offsetHeight;
                        Object5Top = Object5.offsetTop;
                        tintin5Front = tintin.offsetLeft + tintin.offsetWidth - 50;
                        alcazar5Front = alcazar.offsetLeft;
                        tintin5Back = tintin.offsetLeft + 100;
                        alcazar5Back = alcazar.offsetLeft;
                        Object5Front = Object5.offsetLeft;
                        Object5Back = Object5.offsetLeft + Object5.offsetWidth;

                        if (tintin5Bottom > Object5Top & tintin5Front > Object5Front & tintin5Back < Object5Back) {
                            function getHurt() {
                                let tintinHurt = false
                                return setInterval(function () {
                                    if (tintinHurt == false) {
                                        tintinHurt = true
                                        tintin.style.opacity = "0.5";
                                    } else {
                                        tintinHurt = false
                                        tintin.style.opacity = "1";
                                    }
                                }, 350);
                            };

                            var id = getHurt();

                            setTimeout(function () {
                                clearInterval(id);
                                tintin.style.opacity = "1";
                                posx = posx + 5;
                                posLose = posLose + 5;
                            }, 2000);
                        }
                    }, 100);

                };



                var id = Touche5();

                setTimeout(function () {
                    clearInterval(id);
                }, 300000);
            }, 120000);

            setTimeout(function () {
                function SpawnObject6() {
                    timeRandom6 = random(4000, 6000);
                    return setInterval(function () {
                        spriteRandom6 = random(1, 5);
                        if (spriteRandom6 == 1) {
                            Object6.setAttribute('src', '/images/gameTintin/coffre.png')
                        }
                        if (spriteRandom6 == 2) {
                            Object6.setAttribute('src', '/images/gameTintin/licorne.png')
                        }
                        if (spriteRandom6 == 3) {
                            Object6.setAttribute('src', '/images/gameTintin/nainjardin.png')
                        }
                        if (spriteRandom6 == 4) {
                            Object6.setAttribute('src', '/images/gameTintin/pirogue.png')
                        }
                        if (spriteRandom6 == 5) {
                            Object6.setAttribute('src', '/images/gameTintin/vase.png')
                        }
                        Object6.style.right = -400;
                        function MoveObject6() {
                            posObj6 = -400;
                            return setInterval(function () {
                                Object6.style.right = posObj6;
                                posObj6 = posObj6 + 4;
                            }, 1);
                        };
                        var id = MoveObject6();

                        setTimeout(function () {
                            clearInterval(id);
                        }, timeRandom6);
                    }, timeRandom6);
                };
                var id = SpawnObject6();

                setTimeout(function () {
                    clearInterval(id);
                }, 300000);

                function Touche6() {
                    return setInterval(function () {
                        tintin6Bottom = tintin.offsetTop + tintin.offsetHeight;
                        Object6Top = Object6.offsetTop;
                        tintin6Front = tintin.offsetLeft + tintin.offsetWidth - 50;
                        alcazar6Front = alcazar.offsetLeft;
                        tintin6Back = tintin.offsetLeft + 100;
                        alcazar6Back = alcazar.offsetLeft;
                        Object6Front = Object6.offsetLeft;
                        Object6Back = Object6.offsetLeft + Object6.offsetWidth;

                        if (tintin6Bottom > Object6Top & tintin6Front > Object6Front & tintin6Back < Object6Back) {
                            function getHurt() {
                                let tintinHurt = false
                                return setInterval(function () {
                                    if (tintinHurt == false) {
                                        tintinHurt = true
                                        tintin.style.opacity = "0.5";
                                    } else {
                                        tintinHurt = false
                                        tintin.style.opacity = "1";
                                    }
                                }, 350);
                            };

                            var id = getHurt();

                            setTimeout(function () {
                                clearInterval(id);
                                tintin.style.opacity = "1";
                                posx = posx + 5;
                                posLose = posLose + 5;
                            }, 2000);
                        }
                    }, 100);

                };



                var id = Touche6();

                setTimeout(function () {
                    clearInterval(id);
                }, 300000);
            }, 240000);

            setTimeout(function () {
                function SpawnObjectBird() {
                    timeRandomBird = random(30000, 100000);
                    return setInterval(function () {
                        ObjectBird.setAttribute('src', '/images/gameTintin/bird.png')
                        ObjectBird.style.bottom = "40%";
                        function MoveObjectBird() {
                            posObjBird = -400;
                            return setInterval(function () {
                                ObjectBird.style.right = posObjBird;
                                posObjBird = posObjBird + 5;
                            }, 1);
                        };
                        var id = MoveObjectBird();

                        setTimeout(function () {
                            clearInterval(id);
                        }, timeRandomBird);
                    }, timeRandomBird);
                };
                var id = SpawnObjectBird();

                setTimeout(function () {
                    clearInterval(id);
                }, 300000);

                function ToucheBird() {
                    return setInterval(function () {
                        tintinBirdTop = tintin.offsetTop;
                        alcazarBirdTop = alcazar.offsetTop;
                        ObjectBirdBottom = ObjectBird.offsetTop + ObjectBird.offsetHeight / 2;
                        tintinBirdFront = tintin.offsetLeft + tintin.offsetWidth - 30;
                        alcazarBirdFront = alcazar.offsetLeft;
                        tintinBirdBack = tintin.offsetLeft + 30;
                        alcazarBirdBack = alcazar.offsetLeft;
                        ObjectBirdFront = ObjectBird.offsetLeft;
                        ObjectBirdBack = ObjectBird.offsetLeft + ObjectBird.offsetWidth;

                        if (tintinBirdTop < ObjectBirdBottom & tintinBirdFront > ObjectBirdFront & tintinBirdBack < ObjectBirdBack) {
                            function getHurt() {
                                let tintinHurt = false
                                return setInterval(function () {
                                    if (tintinHurt == false) {
                                        tintinHurt = true
                                        tintin.style.opacity = "0.5";
                                    } else {
                                        tintinHurt = false
                                        tintin.style.opacity = "1";
                                    }
                                }, 350);
                            };

                            var id = getHurt();

                            setTimeout(function () {
                                clearInterval(id);
                                tintin.style.opacity = "1";
                                posx = posx + 10;
                                posLose = posLose + 10;
                            }, 2000);
                        }
                    }, 100);

                };



                var id = ToucheBird();

                setTimeout(function () {
                    clearInterval(id);
                }, 300000);
            }, 0);






            //Victoire & défaite
            let alcazarwin = document.createElement('img');
            let alcazarwin2 = document.createElement('img');
            let tintinwin = document.createElement('img');
            let retrybutton = document.createElement('button');
            let acceschateau = document.createElement('button');
            let leavegame = document.createElement('button');

            let retrybutton_txt = document.createTextNode("Rejouer");
            let acceschateau_txt = document.createTextNode("Accéder au chateau");
            let leavegame_txt = document.createTextNode("Quitter le jeu");

            body.appendChild(alcazarwin);
            body.appendChild(alcazarwin2);
            body.appendChild(tintinwin);
            body.appendChild(retrybutton);
            retrybutton.appendChild(retrybutton_txt);
            body.appendChild(acceschateau);
            acceschateau.appendChild(acceschateau_txt);
            body.appendChild(leavegame);
            leavegame.appendChild(leavegame_txt);

            alcazarwin.classList.add('win');
            alcazarwin2.classList.add('win');
            tintinwin.classList.add('win');
            retrybutton.classList.add('hiddenbutton');
            retrybutton.id = 'retrybutton';
            acceschateau.classList.add('hiddenbutton');
            acceschateau.id = 'acceschateau';
            acceschateau.setAttribute('href', '/moulinsart')
            leavegame.classList.add('hiddenbutton');
            leavegame.id = 'leavegame';
            leavegame.setAttribute('href', '/')

            alcazarwin.setAttribute('src', '/images/gameTintin/alcazarwin.png');
            alcazarwin2.setAttribute('src', '/images/gameTintin/alcazarwin2.png');
            tintinwin.setAttribute('src', '/images/gameTintin/tintinwin.png');

            retrybutton.addEventListener("click", () => {
                window.location.reload()
            });

            function LoseWin() {
                return setInterval(function () {
                    let tintinPos = tintin.offsetLeft;
                    let alcazarPos = alcazar.offsetLeft + alcazar.offsetWidth / 2;
                    if (alcazarPos > tintinPos) { //Alcazar attrape tintin
                        alcazarwin.style.display = 'block';
                        document.getElementById('header').style.display = 'none';
                        document.getElementById('track').style.display = 'none';
                        tintin.style.display = 'none';
                        alcazar.style.display = 'none';
                        musicButton.style.display = 'none';
                        Object1.style.display = 'none';
                        Object2.style.display = 'none';
                        Object3.style.display = 'none';
                        Object4.style.display = 'none';
                        Object5.style.display = 'none';
                        Object6.style.display = 'none';
                        ObjectBird.style.display = 'none';
                        setTimeout(function () {
                            retrybutton.style.display = 'block';
                        }, 3000);
                        setTimeout(function () {
                            retrybutton.style.left = '38%';
                        }, 7000);
                        setTimeout(function () {
                            leavegame.style.display = 'block';
                        }, 8000);
                    }
                    if (posLose < 0) { //Alcazar sort de l'écarn (tintin s'est échappé)
                        tintinwin.style.display = 'block';
                        document.getElementById('header').style.display = 'none';
                        document.getElementById('track').style.display = 'none';
                        tintin.style.display = 'none';
                        alcazar.style.display = 'none';
                        musicButton.style.display = 'none';
                        Object1.style.display = 'none';
                        Object2.style.display = 'none';
                        Object3.style.display = 'none';
                        Object4.style.display = 'none';
                        Object5.style.display = 'none';
                        Object6.style.display = 'none';
                        ObjectBird.style.display = 'none';
                        setTimeout(function () {
                            acceschateau.style.display = 'block';
                        }, 3000);
                    }
                }, 100);
            };



            var id = LoseWin();

            setTimeout(function () { //Timer de 5min fini, les gardes attrapent tintin
                clearInterval(id);
                alcazarwin2.style.display = 'block';
                document.getElementById('header').style.display = 'none';
                document.getElementById('track').style.display = 'none';
                tintin.style.display = 'none';
                alcazar.style.display = 'none';
                musicButton.style.display = 'none';
                Object1.style.display = 'none';
                Object2.style.display = 'none';
                Object3.style.display = 'none';
                Object4.style.display = 'none';
                Object5.style.display = 'none';
                Object6.style.display = 'none';
                ObjectBird.style.display = 'none';
                setTimeout(function () {
                    retrybutton.style.display = 'block';
                }, 3000);
                setTimeout(function () {
                    retrybutton.style.left = '38%';
                }, 7000);
                setTimeout(function () {
                    leavegame.style.display = 'block';
                }, 8000);
            }, 300000);


        }

    });

})