var buttonCharacter = document.getElementById('dropdownButton--Character');
var listCharacter = document.getElementById('wikiList--Character');
var buttonPlaces = document.getElementById('dropdownButton--Places');
var listPlaces = document.getElementById('wikiList--Places');
var buttonComic = document.getElementById('dropdownButton--Comic');
var listComic = document.getElementById('wikiList--Comic');
var buttonFilms = document.getElementById('dropdownButton--Films');
var listFilms = document.getElementById('wikiList--Films');
var buttonGame = document.getElementById('dropdownButton--Game');
var listGame = document.getElementById('wikiList--Game');
var buttonCartoon = document.getElementById('dropdownButton--Cartoon');
var listCartoon = document.getElementById('wikiList--Cartoon');

listCharacter.style.height = '125px'
listPlaces.style.height = '125px'
listComic.style.height = '125px'
listFilms.style.height = '125px'
listGame.style.height = '125px'
listCartoon.style.height = '125px'
buttonCharacter.addEventListener("click", characterClick)
buttonPlaces.addEventListener("click", placesClick)
buttonComic.addEventListener("click", comicClick)
buttonFilms.addEventListener("click", filmsClick)
buttonGame.addEventListener("click", gameClick)
buttonGame.addEventListener("click", cartoonClick)

function characterClick(e) {
    if (listCharacter.style.height == '125px') {
        listCharacter.style.height = '100%'
        buttonCharacter.style.transform = "rotate(135deg)";
    } else {
        buttonCharacter.style.transform = "rotate(-45deg)";
        listCharacter.style.height = '125px'
    }
}
function placesClick(e) {
    if (listPlaces.style.height == '125px') {
        listPlaces.style.height = '100%'
        buttonPlaces.style.transform = "rotate(135deg)";
    } else {
        buttonPlaces.style.transform = "rotate(-45deg)";
        listPlaces.style.height = '125px'
    }
}
function comicClick(e) {
    if (listComic.style.height == '125px') {
        listComic.style.height = '100%'
        buttonComic.style.transform = "rotate(135deg)";
    } else {
        buttonComic.style.transform = "rotate(-45deg)";
        listComic.style.height = '125px'
    }
}
function filmsClick(e) {
    if (listFilms.style.height == '125px') {
        listFilms.style.height = '100%'
        buttonFilms.style.transform = "rotate(135deg)";
    } else {
        buttonFilms.style.transform = "rotate(-45deg)";
        listFilms.style.height = '125px'
    }
}
function gameClick(e) {
    if (listGame.style.height == '125px') {
        listGame.style.height = '100%'
        buttonGame.style.transform = "rotate(135deg)";
    } else {
        buttonGame.style.transform = "rotate(-45deg)";
        listGame.style.height = '125px'
    }
}
function cartoonClick(e) {
    if (listCartoon.style.height == '125px') {
        listCartoon.style.height = '100%'
        buttonCartoon.style.transform = "rotate(135deg)";
    } else {
        buttonCartoon.style.transform = "rotate(-45deg)";
        listCartoon.style.height = '125px'
    }
}